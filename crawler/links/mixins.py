import re

from documents.models import Document
from links.models import Link


class RetrieveWebLinksMixin(object):

    def get_web_link(self, page=None):
        if not page:
            return None
        return re.findall(
            'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', page
        )

    def retrieve_all_web_links(self, pages=None):
        self.found_web_links = []

        try:
            iter(pages)
        except TypeError:
            return self.found_web_links

        for page in pages:

            found_links = self.get_web_link(page)
            if found_links:
                self.found_web_links.extend(found_links)


class SaveDocumentsLinksMixin(object):

    def save_documents_with_links(self, name, found_links_with_statuses):
        if not name and found_links_with_statuses:
            return False

        document = Document.objects.create(
            name=name
        )

        for found_link_with_statuse in found_links_with_statuses:
            document.links.create(**found_link_with_statuse)

        return True
