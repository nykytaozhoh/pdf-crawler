from rest_framework import serializers

from documents.models import Document
from links.models import Link


class LinkSerializer(serializers.Serializer):

    id = serializers.CharField(source="get_id")
    url_name = serializers.URLField()
    status = serializers.CharField()

    class Meta:
        model = Link


class ValidateGetLinksApiViewViewGetSerializer(serializers.Serializer):
    id = serializers.CharField(required=True)

    class Meta:
        model = Document

    def validate(self, attrs):
        try:
            return Document.objects.get(id=attrs.get("id"))
        except Document.DoesNotExist:
            raise serializers.ValidationError('Invalid id for document object')


class LinkRelatedDocumentSerializer(serializers.Serializer):
    id = serializers.CharField(source="get_id")
    url_name = serializers.URLField()
    status = serializers.CharField()
    documents_contained = serializers.CharField(source="number_of_documents_url_contained_in")

    class Meta:
        model = Link
