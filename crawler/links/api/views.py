from rest_framework.response import Response
from rest_framework.views import APIView

from links.models import Link

from serializers import (
    LinkSerializer,
    ValidateGetLinksApiViewViewGetSerializer,
    LinkRelatedDocumentSerializer
)


class GetLinksApiView(APIView):

    def get(self, request, *args, **kwargs):

        get_dict = {k: v for k, v in request.GET.items()}

        data = ValidateGetLinksApiViewViewGetSerializer(data=get_dict)
        if data.is_valid():
            document = data.validated_data
            return Response(
                LinkSerializer(
                    document.links.all(), many=True
                ).data
            )
        return Response({"error": data.errors})


class GetAllLinksApiView(APIView):

    def get(self, request, *args, **kwargs):
        return Response(
            LinkRelatedDocumentSerializer(
                Link.objects.all(), many=True
            ).data
        )
