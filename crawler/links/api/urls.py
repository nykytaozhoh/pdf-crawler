from django.conf.urls import url
from views import GetLinksApiView, GetAllLinksApiView


urlpatterns = [
    url(r'^get/', GetLinksApiView.as_view(), name=''),
    url(r'^all/', GetAllLinksApiView.as_view(), name=''),
]
