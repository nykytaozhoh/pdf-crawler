# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('documents', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url_name', models.URLField()),
                ('status', models.CharField(default=b'inactive', max_length=10, choices=[(b'active', b'ACTIVE'), (b'inactive', b'INACTIVE')])),
                ('document', models.ForeignKey(related_name='links', blank=True, to='documents.Document', null=True)),
            ],
        ),
    ]
