from django.db import models

from documents.models import Document


class Link(models.Model):

    LINK_STATUS_OPTIONS = (
        ("active", "ACTIVE"),
        ("inactive", "INACTIVE"),
    )

    url_name = models.URLField(
        null=False,
        blank=False
    )

    document = models.ForeignKey(
        'documents.Document',
        null=True,
        blank=True,
        related_name="links"
    )

    status = models.CharField(
        max_length=10,
        choices=LINK_STATUS_OPTIONS,
        default="inactive"
    )

    def get_id(self):
        return self.id

    def number_of_documents_url_contained_in(self):
        links = Link.objects.filter(url_name=self.url_name)
        return len(set([link.document for link in links]))
