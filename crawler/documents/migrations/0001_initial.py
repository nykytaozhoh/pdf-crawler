# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('type', models.CharField(default=b'pdf', max_length=5, choices=[(b'pdf', b'PDF'), (b'doc', b'DOC'), (b'docx', b'DOCX'), (b'odt', b'ODT')])),
            ],
        ),
    ]
