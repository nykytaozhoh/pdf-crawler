from django.shortcuts import render
from django.views.generic import View

from documents.forms import UploadFileForm
from links.mixins import RetrieveWebLinksMixin, SaveDocumentsLinksMixin
from utils.utils import PDFHandler, CheckLinkActiveStatusHandler

from documents.models import Document


class UploadPDFView(View, RetrieveWebLinksMixin, SaveDocumentsLinksMixin):
    template_name = 'upload_form.html'

    def get(self, request, *args, **kwargs):
        context = dict()
        context['form'] = UploadFileForm()
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):

        context = dict()
        form = UploadFileForm(request.POST, request.FILES)

        if form.is_valid():

            document = form.files["document"]

            pdf_file = PDFHandler(document)
            pages = pdf_file.get_web_links()

            self.retrieve_all_web_links(pages)

            checker = CheckLinkActiveStatusHandler(self.found_web_links)
            found_links_with_statuses = checker.check()

            self.save_documents_with_links(document.name, found_links_with_statuses)

            context['status'] = True

        context['form'] = form

        return render(request, self.template_name, context)
