from django.conf.urls import url

# from documents.views import upload_and_save_links
from documents.views import UploadPDFView


urlpatterns = [
    url(r'^upload/', UploadPDFView.as_view(), name='upload_file'),
]
