from django.db import models


class Document(models.Model):

    DOCUMENT_FORMAT_OPTIONS = (
        ("pdf", "PDF"),
        ("doc", "DOC"),
        ("docx", "DOCX"),
        ("odt", "ODT")
    )

    name = models.CharField(
        max_length=50,
        null=False,
        blank=False
    )

    type = models.CharField(
        max_length=5,
        choices=DOCUMENT_FORMAT_OPTIONS,
        default="pdf"
    )

    def get_id(self):
        return self.id

    def get_number_of_links(self):
        return self.links.count()
