# from rest_framework import status
# from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from documents.models import Document

from serializers import DocumentSerializer


class GetAllDocumentsApiView(APIView):

    def get(self, request, *args, **kwargs):
        return Response(
            DocumentSerializer(
                Document.objects.all(), many=True
            ).data
        )
