from django.conf.urls import url
from views import GetAllDocumentsApiView


urlpatterns = [
    url(r'^all/', GetAllDocumentsApiView.as_view(), name=''),
]

