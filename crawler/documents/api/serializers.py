from rest_framework import serializers

from documents.models import Document


class DocumentSerializer(serializers.Serializer):

    id = serializers.CharField(source="get_id")
    name = serializers.CharField()
    type = serializers.CharField()
    links_number = serializers.CharField(source="get_number_of_links")

    class Meta:
        model = Document
