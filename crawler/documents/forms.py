from django import forms


class UploadFileForm(forms.Form):
    document = forms.FileField()

    def clean(self):
        cleaned_data = super(UploadFileForm, self).clean()

        if not cleaned_data.get("document", None) or not cleaned_data.get("document").content_type == "application/pdf":
            raise forms.ValidationError('Please, provide a file of .pdf format.')

        return cleaned_data
