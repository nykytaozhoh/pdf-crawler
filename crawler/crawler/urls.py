from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [

    url(r'^admin/', include(admin.site.urls)),

    url(r'^documents/', include('documents.urls')),

    # api routes
    url(r'^api/documents/', include('documents.api.urls')),
    url(r'^api/links/', include('links.api.urls')),
]
