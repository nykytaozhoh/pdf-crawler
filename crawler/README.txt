Changed version of django-rest to 3.3 to suit python 2.7.
Decided to use sqlite for easier application and checking.
No authorization provided, since none was specified in requirements.
No tests provided.

Checks for passing only .pdf files are added, however, I'd suggest make limitations by the size of the uploaded file.

In order to start project, please, run: 

git clone https://nykytaozhoh@bitbucket.org/nykytaozhoh/pdf-crawler.git

update the system for texttopdf lib, which is being used for this project
sudo apt-get install build-essential libpoppler-cpp-dev pkg-config python-dev

pip install pipenv

pipenv install

pipenv shell

python manage.py migrate --settings=crawler.settings.settings

python manage.py runserver --settings=crawler.settings.settings

Visit to upload .pdf file to the system:
http://127.0.0.1:8000/documents/upload/

Retrieve data related to the all documents and details in JSON:
http://127.0.0.1:8000/api/documents/all/

Retrieve data related to the all links and details in JSON:
http://127.0.0.1:8000/api/links/all/
Retrieve data related to the specific set of links and a document and their details in JSON:
http://127.0.0.1:8000/api/links/get/?id=14

A couple of times changed library for processing .pdf files. The prettiest and working was pdftotext from pypi repo, however, it caused additional issues later on.

As I figured out deployment via heroku or pythoanywhere failed continiously because of this library. The reason for that consists in a fact that it uses many non-python system dependencies. However, such services don't let make such system updates resulting in failing installation of this package.For now two ideas are active: either use VPS and setup everything by hands or setup heroku with pre-installation options and try update system enviroment before doing any python packages.

