import pdftotext
import requests


class PDFHandler(object):

    def __init__(self, document=None, *args, **kwargs):
        self.document = document

    def get_web_links(self):
        file_to_read = self.document
        pdf_file = pdftotext.PDF(file_to_read)
        file_to_read.close()
        return (page.encode("utf-8") for page in pdf_file)


class CheckLinkActiveStatusHandler(object):

    def __init__(self, links=None):
        self.links = links

    def check(self):
        checked_links = []

        try:
            iter(self.links)
        except TypeError:
            return []

        for link in self.links:
            request = requests.get(link)
            checked_links.append({'status': 'active', 'url_name': link, })\
                if request.status_code == 200 else checked_links.append({'inactive', link})
        return checked_links
